-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 02, 2018 at 01:21 PM
-- Server version: 5.6.25-73.0-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pixolpvd_fcbescola`
--

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `logo` text NOT NULL,
  `initials` varchar(100) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `logo`, `initials`, `parent_id`) VALUES
(88, 'Valiente FC Girls U19', 'fcbescola1527753235valiente-football-club.png', 'Valiente', NULL),
(87, 'GIFA Girls U19', 'fcbescola1527753193gifa.png', 'GIFA', NULL),
(97, 'FCBEscola Mumbai BLAU Girls U19', 'fcbescola1527836698fcbes-03-01-01.png', 'FCB', NULL),
(96, 'FCBEscola Mumbai BLAU U19', 'fcbescola1527836724fcbes-03-01-01.png', 'FCB BLAU', NULL),
(86, 'Kopana Football School Girls U14', 'fcbescola1527753145kopana.png', 'Kopana', NULL),
(95, 'CFCI Girls U14', 'fcbescola1527753626community-football-club-of-india.png', 'CFCI', NULL),
(94, 'St. Augustine School Girls U14', 'fcbescola1527753544st.-augustine-school.png', 'St. Augustine School', NULL),
(93, 'Bravehearts Football Skool Girls U14', 'fcbescola1527753497bravehearts-football-school.png', 'Bravehearts', NULL),
(92, 'Kopana Football School Girls U19', 'fcbescola1527753439kopana.png', 'KOPANA', NULL),
(91, 'FCM Girls U14', 'fcbescola1527753400fc-mumbaikars.png', 'FCM', NULL),
(89, 'KNFC Girls U19', 'fcbescola1527753289kapadia-nagar-football-club-knfc.png', 'KNFC', NULL),
(90, 'Soccer United Girls U19', 'fcbescola1527753337soccer-united.png', 'soccer United', NULL),
(102, 'FCM U10', 'fcbescola1527775181fc-mumbaikars.png', 'TRFA', NULL),
(82, 'FCBEscola Mumbai BLAU U8', 'fcbescola1527836764fcbes-03-01-01.png', 'FCBEscola Mumbai', NULL),
(81, 'Kopana Football School HAZARD U10', 'fcbescola1527623482kopana-logo.jpg', 'Kopana Hazard', NULL),
(63, 'Western India United FC U10', 'fcbescola1527621733wufc_logo.png', 'WUFC', NULL),
(62, 'Valiente FC U14', 'fcbescola1527621560valiente.gif', 'Valiente FC', NULL),
(61, 'Boca Juniors Football School India U8', 'fcbescola1527775215boca-juniors.png', 'Boca', NULL),
(57, 'FCM U8', 'fcbescola1527775235fc-mumbaikars.png', 'FCM', NULL),
(103, 'FCBEscola Mumbai BLAU U16', 'fcbescola1527836781fcbes-03-01-01.png', 'FCB', NULL),
(60, 'CFCI U8', 'fcbescola1527621303cfci-logo.jpg', 'CFCI', NULL),
(58, 'Kopana Football School U8', 'fcbescola1527619508kopana-logo.jpg', 'Kopana', NULL),
(64, 'Bhaichung Bhutia Football Schools U12', 'fcbescola1527621886bbfs.jpg', 'BBFS', NULL),
(65, 'Sporko CFCI U12', 'fcbescola1527865486community-football-club-of-india.png', 'IOT', NULL),
(66, 'Bravehearts Football Skool BLUE U8', 'fcbescola1527622086bravehearts-01-1.jpg', 'Bravehearts', NULL),
(67, 'VFC U10', 'fcbescola1527622192vfc-05.jpg', 'VFC', NULL),
(68, 'Valiente FC U19', 'fcbescola1527833094valiente-football-club.png', 'KNFC', NULL),
(69, 'The Soccer Academy U8', 'fcbescola1527622534tsa.jpg', 'TSA', NULL),
(70, 'TRFA U19', 'fcbescola1527622608trfa.jpg', 'TRFA', NULL),
(71, 'Boca Juniors Football School India U10', 'fcbescola1527775295boca-juniors.png', 'Soccer United', NULL),
(72, 'Iron Born FC U16', 'fcbescola1527622746iron-born-fc.jpg', 'Iron Born', NULL),
(73, 'CJ Football Academy U14', 'fcbescola1527622820cj-football-academy.jpg', 'CJFA', NULL),
(74, 'Samuel Football Academy U14', 'fcbescola1527622918sfa.jpeg', 'SFA', NULL),
(75, 'CFCI U10', 'fcbescola1527775329community-football-club-of-india.png', 'St. Augustine', NULL),
(76, 'FCBEscola Mumbai GRANA U8', 'fcbescola1527836835fcbes-03-01-01.png', 'FCBE Grana', NULL),
(77, 'FCBEscola Mumbai GROC U12', 'fcbescola1527836917fcbes-03-01-01.png', 'FCBE Groc', NULL),
(78, 'FCBEscola Mumbai BLAU U12', 'fcbescola1527836950fcbes-03-01-01.png', 'FCBE', NULL),
(79, 'Bravehearts Football Skool RED U8', 'fcbescola1527623379bravehearts-01-1.jpg', 'Bravehearts Red', NULL),
(80, 'Kopana Football School OZIL U10', 'fcbescola1527775392kopana.png', 'Bravehearts Blue', NULL),
(56, 'FCBEscola Mumbai BLAU U10', 'fcbescola1527836963fcbes-03-01-01.png', 'FCBE Blau', NULL),
(59, 'GIFA U8', 'fcbescola1527619583gifa.jpg', 'GIFA', NULL),
(98, 'FCBEscola Mumbai GRANA U19', 'fcbescola1527836978fcbes-03-01-01.png', 'FCB', NULL),
(99, 'FCM U19', 'fcbescola1527775437fc-mumbaikars.png', 'fCM', NULL),
(101, 'CFCI U19', 'fcbescola1527754076community-football-club-of-india.png', 'CFCI', NULL),
(104, 'FCM U16', 'fcbescola1527754194fc-mumbaikars.png', 'FCM', NULL),
(105, 'Kopana Football School U16', 'fcbescola1527754220kopana.png', 'Kopana', NULL),
(106, 'GIFA U16', 'fcbescola1527754244gifa.png', 'GIFA', NULL),
(107, 'CFCI U16', 'fcbescola1527754273community-football-club-of-india.png', 'CFCI', NULL),
(109, 'FCBEscola Mumbai BLAU U14', 'fcbescola1527836999fcbes-03-01-01.png', 'FCB Blau', NULL),
(110, 'Rising Stars FC', 'fcbescola1527869543rising-stars-logo-01.png', 'IOT', NULL),
(111, 'Kopana Football School U14', 'fcbescola1527754634kopana.png', 'Kopana', NULL),
(112, 'CFCI U14', 'fcbescola1527754668community-football-club-of-india.png', 'CFCI', NULL),
(113, 'Boca Juniors Football School India U14', 'fcbescola1527754748boca-juniors.png', 'Boca', NULL),
(114, 'VFC U14', 'fcbescola1527754771vfc.png', 'VFC', NULL),
(115, 'FCBEscola Mumbai GRANA U14', 'fcbescola1527837018fcbes-03-01-01.png', 'FCB Grana', NULL),
(117, 'Valiente FC U12', 'fcbescola1527754869valiente-football-club.png', 'Valiente FC', NULL),
(119, 'FCM U14', 'fcbescola1527755005fc-mumbaikars.png', 'FCM', NULL),
(120, 'Western India United FC U14', 'fcbescola1527755030western-united-football-club.png', 'Western India United', NULL),
(121, 'FCBEscola Mumbai GRANA U12', 'fcbescola1527837048fcbes-03-01-01.png', 'FCB Grana', NULL),
(122, 'FCM U12', 'fcbescola1527755193fc-mumbaikars.png', 'FCM', NULL),
(123, 'Kopana Football School U12', 'fcbescola1527755232kopana.png', 'Kopana', NULL),
(124, 'Boca Juniors Football School India U12', 'fcbescola1527755262boca-juniors.png', 'Boca', NULL),
(126, 'FCBEscola Mumbai GRANA U10', 'fcbescola1527837065fcbes-03-01-01.png', 'FCB BLAU', NULL),
(129, 'VFC U12', 'fcbescola1527755945vfc.png', 'VFC', NULL),
(130, 'Western India United FC U12', 'fcbescola1527756041western-united-football-club.png', 'Western India United', NULL),
(131, 'CFCI U12', 'fcbescola1527883734community-football-club-of-india.png', 'IOT', NULL),
(132, 'GIFA U10', 'fcbescola1527775497gifa.png', 'FCB Blau', NULL);

--
-- Triggers `teams`
--
DELIMITER $$
CREATE TRIGGER `after_teams_delete` AFTER DELETE ON `teams` FOR EACH ROW BEGIN
	DELETE FROM `user_followteam` WHERE `team_id` = OLD.id;
    DELETE FROM `team_fixture` WHERE `team_id` = OLD.id;
    DELETE FROM `team_group` WHERE `team_id` = OLD.id;
    DELETE FROM `results` WHERE `team_id` = OLD.id;
END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
