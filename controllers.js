var imageurl = "http://pixoloproductions.com/fcb/fcbescolarest/uploads/";
angular.module('starter.controllers', [])

.controller('AppCtrl', function ($scope, $ionicModal, $timeout, $location, $rootScope, ApiServices) {

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    $scope.$on('$ionicView.enter', function (e) {
        var path = $location.path();

        $rootScope.showtabs = (path != '/app/login');
        console.log($rootScope.showtabs);
    });

    // Form data for the login modal

    //        $ionicConfigProvider.views.transition('none');


    /*GET INFORMATIONS FOR FIXTURES */

    $rootScope.fcbuserdata = $.jStorage.get('fcbuserdata');

    getdataforfixturessuccess = function (response) {
        console.log(response);
        $rootScope.dataforfixture = response.data;
        $.jStorage.set('dataforfixture', response.data);

    }

    getdataforfixtureserror = function (error) {
        console.log(error);
    }



    ApiServices.getdataforfixtures().then(getdataforfixturessuccess, getdataforfixtureserror);


    $rootScope.dataforfixture = $.jStorage.get("dataforfixture");


    // SET USER DATA IN ROOT SCOPE
    if ($.jStorage.get("fcbuserdata") != undefined) {
        $rootScope.fcbuserdata = $.jStorage.get("fcbuserdata");
    } else {
        $rootScope.fcbuserdata = {
            name: "FCB User",
            facebook_profile_photo: "http://pixoloproductions.com/fcb/fcb_default_profile.svg",
            facebook_access_token: ""
        };
    };


    $scope.logout = function () {
        $.jStorage.deleteKey('fcbuserdata');
        $location.path('app/login');
    }
})

.controller('PlaylistsCtrl', function ($scope) {})

.controller('PlaylistCtrl', function ($scope, $stateParams) {})



.controller('albumCtrl', function ($scope, $rootScope, $location, $interval, $ionicLoading, $stateParams, ApiServices) {
        /*INITIALIZATIONS*/
        $scope.albums = [];


        getalbumsuccess = function (response) {
            console.log(response);
            $scope.albums = response.data;
        }
        getalbumerror = function (error) {
            console.log(error);
        }



        /*Scope Functions*/
        $scope.getphotos = function (album) {
            console.log(album);
            $location.path('app/photos/' + album.id);
        }

        /**/
        ApiServices.getalbum().then(getalbumsuccess, getalbumerror);

    })
    .controller('storiesCtrl', function ($scope, $rootScope, $location, $interval, $ionicLoading, $stateParams, ApiServices) {

    })
    .controller('fixturesCtrl', function ($scope, $rootScope, $location, $interval, $ionicLoading, $stateParams, ApiServices) {

        /*INITIALIZATION*/
        var tournamentid = '24';
        $scope.fixtures = []

        // GET FIXTURES BY TOURNAMENT ID
        getfixturesbytournamentidsuccess = function (response) {
            //$scope.fixtures = response.data;

            console.log(response.data);

            var lastdate = "";
            var lastcategoryid = "";

            var datearray = [];
            $scope.datesortedfixtures = [];

            $scope.sortedarray = [];

            console.log(response.data);

            response.data.forEach(function (value, key) {

                if (lastdate != value.date) {
                    $scope.sortedarray.push({
                        date: value.date,
                        categories: []
                    });
                    lastdate = value.date;
                    lastcategoryid = "";
                };

                if (lastcategoryid != value.category_id) {
                    $scope.sortedarray[$scope.sortedarray.length - 1].categories.push({
                        category: value.category_name,
                        fixtures: []
                    });

                    lastcategoryid = value.category_id;
                };


                sl = $scope.sortedarray.length - 1;
                cl = $scope.sortedarray[$scope.sortedarray.length - 1].categories.length - 1;

                $scope.sortedarray[sl].categories[cl].fixtures.push(value);

                $scope.fixtures = response.data;
                console.log($scope.fixtures);
                $scope.fixtures.forEach(function (value, key) {

                });

                console.log($scope.sortedarray);

                setdateslider();

                /*console.log($scope.sortedarray)

                response.data.forEach(function (value, key) {


                    if (lastdate != value.date) {

                        if (datearray.length > 0) {
                            $scope.datesortedfixtures.push(datearray);
                        };

                        lastdate = value.date;

                        datearray = [];

                    };

                    datearray.push(value);


                });

                console.log($scope.datesortedfixtures);
                var catarray = [];
                var datearray
                $scope.catsortedarray = [];
                var lastcat = "";*/


                /*$scope.datesortedfixtures.forEach(function (value, key) {
                    value.forEach(function (value2, key) {
                        if (lastcat != value2.category_id) {
                            if (catarray.length > 0) {
                                $scope.catsortedarray.push(catarray);
                            };
                            lastcat = value2.category_id;
                            catarray = [];
                        };
                        catarray.push(value2);
                    });
                });*/


            });
        };
        getfixturesbytournamentiderror = function (error) {
            console.log(error);
        };



        ApiServices.getfixturesbytournamentid(tournamentid).then(getfixturesbytournamentidsuccess, getfixturesbytournamentiderror);



        /* FIXTURES EFFECT */

        console.log(document.getElementById('dates-slider').children);
        var children;

        var setdateslider = function () {
            setTimeout(function () {
                children = document.getElementById('dates-slider').children;
                changedatesliderpositions(0);

                $('.scroll-content').scroll(function () {
                    $("#full-date-fixture-item .date-wrapper").each(function () {
                        /*console.log($(this).offset().top);
                        console.log($('.scroll-content').scrollTop());*/
                        if (!autoscrolling) {
                            if ($(this).offset().top < 80) {
                                var idstring = $(this).attr('id');
                                var newind = parseInt(idstring.substring(5, idstring.length));
                                changedatesliderpositions(newind);
                            };
                        };
                    });
                });
            }, 0);
        };

        var removeallclassess = function (el) {
            el.classList.remove("active", "prev", "next", "out-left", "out-right");
        };

        var changedatesliderpositions = function (ind) {

            for (var ds = 0; ds < children.length; ds++) {

                //DONT GO THROUGH LOOP
                if (ds == (ind + 1)) {
                    continue;
                };

                //MAKE ACTIVE AND NEXT
                if (ds == ind) {
                    removeallclassess(children[ds]);
                    children[ds].classList.add('active');
                    if (ds + 1 < children.length) {
                        removeallclassess(children[ds + 1]);
                        children[ds + 1].classList.add('next');
                    };
                };

                // SET PREVIOUS
                if ((ds + 1) == ind) {
                    removeallclassess(children[ds]);
                    children[ds].classList.add('prev');
                };

                // SEND TO LEFT
                if (ds < (ind - 1)) {
                    removeallclassess(children[ds]);
                    children[ds].classList.add('out-left');
                };

                // SEND TO RIGHT
                if (ds > ind) {
                    removeallclassess(children[ds]);
                    children[ds].classList.add('out-right');
                };



            };
            return true;
        };

        var scrolltodate = function (ind) {
            console.log($("#date-" + ind).offset().top);
            $('.scroll-content').animate({
                scrollTop: ($(".scroll-content").scrollTop() + ($("#date-" + ind).offset().top - 65))
            }, 2000);
            return true;
        };

        var autoscrolling = false;
        $scope.changeSlider = function (ind) {
            console.log("CLICKED");
            autoscrolling = true;
            changedatesliderpositions(ind);
            scrolltodate(ind);
            autoscrolling = false;
        };


    })
    .controller('loginCtrl', function ($scope, $rootScope, $location, $interval, $ionicLoading, $stateParams, ApiServices, $cordovaDevice, $cordovaOauth) {


        /*INITIALIZATION*/
        var data = {}; //       


        var userdata = {
            name: "",
            email: "",
            password: "",
            device_id: "",
            facebook_access_token: "",
            facebook_id: "",
            facebook_profile_photo: ""
        }



        /*CALLBACK*/

        // STORE THE USERS DATA IN DATABASE
        var checkbeforeloggedinsuccess = function (response) {
            console.log(response.data);
            userdata.id = response.data.id;
            if (userdata.name == '') {
                userdata.name = 'Player ' + response.data.id;
            }
            console.log(userdata);
            // STORE FCBUSERDATA IN JSTORAGE
            $.jStorage.set("fcbuserdata", userdata);

            // STORE IN ROOT SCOPE FOR APP USE
            $rootScope.fcbuserdata = userdata;

            // REDIRECT TO TIMELINE SINCE FACEBOOK LOGIN IS COMPLETE
            $location.path('/app/timeline');
        };
        var checkbeforeloggedinerror = function (response) {
            console.log(response.data);

        };


        //        userdata.device_id = $cordovaDevice.getUUID();
        $scope.facebooklogin = function () {
            $cordovaOauth.facebook("1968132213517198", ["email", 'user_likes', 'user_photos', 'user_posts', 'user_events']).then(function (result) {
                // USER LOGGED IN TO FACEBOOK SUCCESSFULLY
                console.log(result); //                $scope.data = result.access_token;
                userdata.facebook_access_token = result.access_token;



                // GET ALL INFORMATION FROM FACEBOOK USING THE ACCESS TOKEN
                var getfacebookuserdetailssuccess = function (response) {

                    userdata.name = response.data.name;
                    userdata.email = response.data.email;
                    userdata.facebook_id = response.data.id;
                    userdata.facebook_profile_photo = response.data.picture.data.url;

                    // GET DEVICE ID OF PHONE FROM CORDOVA
                    if (window.location.host == "localhost:8100") {
                        userdata.device_id = "device";
                    } else {
                        userdata.device_id = $cordovaDevice.getUUID();
                    };

                    console.log(userdata);
                    //CHECK IF USER IS LOGGED IN VIA FACEBOOK BEFORE OR NOT
                    ApiServices.checkbeforeloggedin(userdata).then(checkbeforeloggedinsuccess, checkbeforeloggedinerror);
                    //                    ApiServices.storepostdata('users', userdata).then(storepostdatasuccess, storepostdataerror);


                };
                var getfacebookuserdetailserror = function (response) {
                    console.log(response.data);
                    // INTERNET ERROR
                };
                ApiServices.getfacebookuserdetails(result.access_token).then(getfacebookuserdetailssuccess, getfacebookuserdetailserror);



            }, function (error) {
                alert("Error : " + error);
            });
        };


        //FACEBOOK LOGIN SKIPPED FUNCTION
        $scope.loginskip = function () {
            ApiServices.checkbeforeloggedin(userdata).then(checkbeforeloggedinsuccess, checkbeforeloggedinerror);
            $location.path('/app/fixtures');
        }


        storepostdatasuccess = function (response) {
            console.log(response);
            userdata.id = response.data;
            $.jStorage.set('fcbuserdata', userdata);

        }
        storepostdataerror = function (error) {
            console.log(error);
        }




    })
    .controller('pointstableCtrl', function ($scope, $rootScope, $location, $interval, $ionicLoading, $stateParams, ApiServices) {

    })
    .controller('addstoryCtrl', function ($scope, $rootScope, $location, $interval, $ionicLoading, $stateParams, ApiServices, $ionicPlatform, $cordovaCamera, $cordovaSocialSharing) {
        console.log(imageurl);

        /*INITIALIZATION*/
        $scope.post = {
            caption: '',
            image: '',
            video: '',
            user_id: $.jStorage.get('fcbuserdata').id,

        };


        $scope.sensoredwords = [];
        $scope.fileURI = '';
        var pictureSource; // picture source
        var destinationType; // sets the format of returned value
        $scope.toggle = {};

        //            FileTransfer Object
        var ft;


        function onDeviceReady() {
            console.log('device ready');
            pictureSource = navigator.camera.PictureSourceType;
            destinationType = navigator.camera.DestinationType;
            ft = new FileTransfer();


        };



        /* var findsensoredwords = function () {



         };*/



        getalldatasuccess = function (response) {
            console.log(response);
            $scope.sensoredwords = response.data;
        }
        getalldataerror = function (error) {
            console.log(error);
        }


        /*Get all Sensored words*/



        ApiServices.getalldata('Cusswords').then(getalldatasuccess, getalldataerror);

        /*SHARE ON FACEBOOK*/
        var sharepostonfacebook = function () {
            console.log(ionic.Platform.device());
            if ($scope.toggle.value) {

                if ($scope.post.caption != '' || $scope.fileURI != '') {
                    console.log($scope.post.caption);
                    $cordovaSocialSharing.shareViaFacebook($scope.post.caption, $scope.fileURI, "", $scope.post.caption)
                        .then(function (result) {
                            // Success!
                            console.log(result);
                        }, function (err) {
                            // An error occurred. Show a message to the user
                            console.log(err);
                        });

                } else {
                    ApiServices.showtoast("Can't post empty message", 'short', 'bottom');
                }




            }
            $scope.fileURI = '';
            $location.path('app/timeline');

        };
        document.addEventListener("deviceready", onDeviceReady, false);


        var submitpost = function () {
            console.log('postdata', $scope.post);

            storepostdatasuccess = function (response) {
                console.log(response);
                sharepostonfacebook();

            }

            storepostdataerror = function (error) {
                console.log(error);
            }


            /*CHECK IF POST CONTAINS SENSORED WORDS*/
            var wordfound = false;
            console.log($scope.sensoredwords.length);
            for (var index in $scope.sensoredwords) {

                console.log(index);
                console.log(new RegExp($scope.sensoredwords[index].word));
                var word = $scope.post.caption.match(new RegExp($scope.sensoredwords[index].word, "i"));
                if (word) {

                    console.log('In if ', word);
                    wordfound = true;
                    break;
                }

            };
            if (!wordfound) {
                console.log('Great gentleman !');
                ApiServices.storepostdata('stories', $scope.post).then(storepostdatasuccess, storepostdataerror);
            } else {

                //            SHOW POPUP HERE FOR USING BAD WORDS
                ApiServices.showpopup("You are getting mischievous");
                console.log('Word found');
            }
            //           

        };

        function clearCache() {
            navigator.camera.cleanup();
        }

        var onCapturePhoto = function (fileURI) {
            console.log(fileURI);
            $scope.fileURI = fileURI;

            /*CALL TO OTHER FUNCTION*/
            $scope.imageuploaded = true;
            $scope.showimg = true;

            var image = document.getElementById('mypostimage');
            console.log(image);
            if (image != null) {

                image.src = $scope.fileURI;
                $scope.$apply();
            };

            clearCache();

        };
        var onFail = function (message) {
            alert('Failed because: ' + message);
        };
        /*PHOTOS UPLOAD FROM CAMERA*/
        $scope.photouploadfromcamera = function () {

            /*FOR ANDROID */
            if (ionic.Platform.isAndroid()) {
                console.log(destinationType);
                navigator.camera.getPicture(onCapturePhoto, onFail, {
                    quality: 30,
                    destinationType: destinationType.FILE_URI,
                    allowEdit: true,
                    correctOrientation: true
                });

            }

        };







        /*PHOTOS UPLOAD FROM GALLERY*/
        $scope.photouploadfromgallery = function () {
            /*FOR ANDROID */
            if (ionic.Platform.isAndroid()) {
                navigator.camera.getPicture(onCapturePhoto, onFail, {
                    quality: 30,
                    destinationType: destinationType.FILE_URI,
                    sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                    allowEdit: true,
                    correctOrientation: true
                });
            }
        };


        //    CANCEL POSTING
        $scope.cancelposting = function () {
            $location.path('app/timeline');
        }
        $scope.removeimage = function () {
            $scope.fileURI = '';
            var image = document.getElementById('mypostimage');
            image.src = '';
            $scope.$apply();

        }


        //    SUBMIT POST
        $scope.transferfile = function () {
            /*IF IMAGE IS UPLOADED*/
            if ($scope.fileURI != '') {
                console.log('transferfile', ft);
                // UPLOAD FILE SUCCESS
                var win = function (data, status) {

                    console.log('data', data);
                    $scope.post.image = data.response;
                    submitpost();
                };
                //UPLOAD FILE FAILURE
                var fail = function (error) {
                    console.log('error', error);
                };

                console.log($scope.fileURI);

                $scope.imgname = angular.copy($scope.fileURI.substr($scope.fileURI.lastIndexOf('/') + 1));
                $scope.imgname = $scope.imgname.substring(0, $scope.imgname.lastIndexOf('?'));
                var options = new FileUploadOptions();
                options.fileKey = "image";
                options.fileName = $scope.imgname;
                options.mimeType = "image/jpeg";

                var params = {};
                params.value1 = "test";
                params.value2 = "param";

                options.params = params;
                options.chunkedMode = false;

                console.log($scope.fileURI);
                console.log(options);
                ft.upload($scope.fileURI, encodeURI("http://pixoloproductions.com/fcb/fcbescolarest/index.php/teams/uploadImage"), win, fail, options, true);

            } else {
                submitpost();
            }


        }

    })

.controller('categoriesCtrl', function ($scope, $rootScope, $location, $interval, $ionicLoading, $stateParams, ApiServices) {

    var tournament_id = 24;


    getalldatasuccess = function (response) {
        console.log(response);
        $scope.categories = response.data;
    }

    getalldataerror = function (error) {
        console.log(error);
    }


    $scope.$on('$ionicView.enter', function () {
        $scope.categories = [];



        ApiServices.getmanyby('categories', 'tournament_id', tournament_id).then(getalldatasuccess, getalldataerror);

    });






})

.controller('timelineCtrl', function ($scope, $rootScope, $location, $interval, $ionicLoading, $stateParams, ApiServices, $ionicScrollDelegate, $cordovaInAppBrowser) {

        console.log($rootScope.fcbuserdata);
        /*INITIALIZATION*/
        $scope.timelineposts = [];
        $scope.platform = ionic.Platform.platform();
        $scope.showlikebutton = false;
        console.log($scope.platform)
            // GET LIST OF ALL THE PLAGES LOKE BY THE USER ON FACEBOOK
        var getlikedpagessuccess = function (response) {
            //            FCBEscola Mumbai
            console.log(response.data.data);

            var index = response.data.data.findIndex(likedpage => likedpage.name.toLowerCase() == 'FCBEscola Mumbai'.toLowerCase());


            console.log(index);
            if (index != -1) {
                $scope.showlikebutton = false;
            } else {
                $scope.showlikebutton = true;
            };
        };
        var getlikedpageserror = function (response) {
            console.log(response.data);
        };
        // CHECK IF ACCESS TOKEN EXISTS
        if ($rootScope.fcbuserdata.facebook_access_token != '') {
            $scope.showloginbutton = false;
            //            ApiServices.getlikedpages($rootScope.fcbuserdata.facebook_access_token).then(getlikedpagessuccess, getlikedpageserror);
        } else {
            $scope.showloginbutton = true;
        };


        var gettimelinepost = function () {
            console.log('getting post');
            $scope.showspinner = true;
            getallstoriessuccess = function (response) {

                console.log(response);
                $scope.timelineposts = response.data;
                setTimeout(function () {
                    console.log('settimeout');
                    $scope.showspinner = false;
                    $scope.$apply();
                }, 1000);

            }

            getallstorieserror = function (error) {
                $scope.showspinner = false;
                console.log(error);
            }
            ApiServices.getallstories().then(getallstoriessuccess, getallstorieserror);
        }

        /*SCOPE FUNCTIONS*/
        $scope.getstories = function () {
            if ($ionicScrollDelegate.getScrollPosition().top == 0) {
                gettimelinepost();
            }

        };
        $scope.sharepost = function (post) {
            console.log(post);
        };
        $scope.deletepost = function (post) {
            console.log(post);
            deletedatabyidsuccess = function (response) {
                console.log(response);
                if (response.data) {
                    $scope.timelineposts.splice($scope.timelineposts.indexOf(post), 1);

                }
            };

            deletedatabyiderror = function (error) {
                console.log(error);
            };

            ApiServices.deletedatabyid('stories', post.id).then(deletedatabyidsuccess, deletedatabyiderror);
        };
        $scope.reportpost = function (post) {

            reportstorysuccess = function (response) {
                console.log(response);
            }

            reportstoryerror = function (error) {
                console.log(error);
            }
            ApiServices.reportstory(post.id).then(reportstorysuccess, reportstoryerror);
            console.log(post);
        };




        $scope.gotoaddstorypage = function () {
            /*Check if user is logged in or not via facebook */
            if ($.jStorage.get('fcbuserdata') && $.jStorage.get('fcbuserdata').facebook_id != '') {
                $location.path('app/addstory');
            } else {
                console.log('You are not logged in via facebook !');
            }

        };


        $scope.$on('$ionicView.enter', function () {
            gettimelinepost();

        });

        var options = {
            location: 'yes',
            clearcache: 'yes',
            toolbar: 'no'
        };
        $rootScope.$on('$cordovaInAppBrowser:exit', function (e, event) {
            console.log(e);
            console.log(event);
        });


        $scope.openpagetolike = function () {
            $cordovaInAppBrowser.open('https://www.facebook.com/v3.0/plugins/error/confirm/like?iframe_referer=http%3A%2F%2Flocalhost%3A8100%2F&kid_directed_site=false&secure=true&plugin=like&return_params=%7B%22action%22%3A%22like%22%2C%22app_id%22%3A%22626608564344145%22%2C%22channel%22%3A%22http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FRQ7NiRXMcYA.js%3Fversion%3D42%23cb%3Dfbe83976122ec4%26domain%3Dlocalhost%26origin%3Dhttp%253A%252F%252Flocalhost%253A8100%252Ff2e190d6d15e614%26relation%3Dparent.parent%22%2C%22container_width%22%3A%22360%22%2C%22href%22%3A%22https%3A%2F%2Fwww.facebook.com%2FFCBEscolaMUMB%2F%22%2C%22layout%22%3A%22box_count%22%2C%22locale%22%3A%22en_US%22%2C%22sdk%22%3A%22joey%22%2C%22share%22%3A%22true%22%2C%22show_faces%22%3A%22true%22%2C%22size%22%3A%22large%22%2C%22ret%22%3A%22sentry%22%2C%22act%22%3A%22like%22%7D', '_blank', options)

            .then(function (event) {
                // success
                console.log(event);
            })

            .catch(function (event) {
                // error
                console.log(event);
            });


        }



    })
    .controller('groupsCtrl', function ($scope, $rootScope, $location, $interval, $ionicLoading, $stateParams, ApiServices) {


        var categoryid = $stateParams.categoryid;
        $scope.pointstabledata = [];



        getdataforpointstablesuccess = function (response) {
            console.log(response);
            $scope.pointstabledata = response.data;
        }

        getdataforpointstableerror = function (error) {
            console.log(error);
        }



        ApiServices.getdataforpointstable(categoryid).then(getdataforpointstablesuccess, getdataforpointstableerror);

    })


.controller('teamsCtrl', function ($scope, $rootScope, $location, $interval, $ionicLoading, $stateParams, ApiServices) {

    /*INITIALIZATION*/
    $scope.teams = [];
    $scope.userfollowteam = {};



    /*GET ALL TEAMS FROM INFORMATION FOR FIXTURES*/
    var setteamsarray = function () {
        console.log($rootScope.dataforfixture);
        _.forEach($rootScope.dataforfixture, function (dataforfixturevalue, dataforfixtureindex) {

            _.forEach(dataforfixturevalue.groups, function (groupsvalue, groupsindex) {
                $scope.teams = $scope.teams.concat(groupsvalue.teams);
            });
            console.log($scope.teams);
        });

    };

    //        setteamsarray();


    /*CALLBACKS*/


    /*   getalldatasuccess = function (response) {
           console.log(response);
           $scope.teams = response.data;

       }
       getalldataerror = function (error) {
           console.log(error);
       }*/

    /*SCOPE FUNCTIONS*/
    var followanimation = function (team, userfollowid, eventtarget) {
        console.log(eventtarget);

        var element = eventtarget.target;



        //SET FOLLOWING VALUE OF TEAM AS TRUE HERE
        $(element).css('color', 'white');
        $(element).html('')
        $(element).css({
            'padding': '0',
            'width': '14px'
        });

        $(element).addClass('continous-rotate');

        var self = $(element);
        var tickid = $(element).data('tickid');


        //CALL THIS DURING SUCCESS
        setTimeout(function () {
            console.log("ÁAA");
            self.fadeOut();
            console.log(tickid);
            setTimeout(function () {

                $('#' + tickid).show();
                $('#' + tickid).css({
                    'height': '35px',
                    'width': '35px',
                    'transform': 'rotateZ(360deg)',
                    'opacity': '1'
                })

            }, 400);
            $scope.userfollowteam[team.id].id = userfollowid;
        }, 800);
        //END OF SUCCESS







    }









    $scope.followteam = function (team, eventtarget) {

        $scope.userfollowteam[team.id] = {
            user_id: $.jStorage.get('fcbuserdata').id,
            team_id: team.id
        };

        storepostdatasuccess = function (response) {
            console.log(response);
            followanimation(team, response.data, eventtarget);

        }
        storepostdataerror = function (error) {
            console.log(error);
        }

        //STORE DATA IN USER_FOLLOW TABLE

        ApiServices.storepostdata('User_followteam', $scope.userfollowteam[team.id]).then(storepostdatasuccess, storepostdataerror);






    };





    var unfollowanimation = function (teamid, event) {

        var element = angular.copy(event.path[2]);

        var tickid = $(element).attr('id');


        $('*[data-tickid="' + tickid + '"]').css('color', 'white');
        $('*[data-tickid="' + tickid + '"]').html('')
        $('*[data-tickid="' + tickid + '"]').css({
            'padding': '0',
            'width': '14px'
        });

        $('*[data-tickid="' + tickid + '"]').addClass('continous-rotate');



        console.log(event);
        //            var element = event.target;

        var self = $(element);

        console.log(self);
        $('#' + tickid).css({
            'height': '0px',
            'width': '0px',
            'transform': 'rotateZ(720deg)',
            'opacity': '0'
        });


        setTimeout(function () {
            self.hide();
            $(element).css('color', 'white');
            $(element).html('');
            $(element).css({
                'padding': '0',
                'width': '14px'
            });
            $(element).addClass('continous-rotate');

            $('*[data-tickid="' + tickid + '"]').show();


            // CALL ON SUCCESS
            setTimeout(function () {
                $('*[data-tickid="' + tickid + '"]').removeClass('continous-rotate');

                $('*[data-tickid="' + tickid + '"]').css({
                    'color': '#000',
                    'padding': '3px 20px',
                    'width': 'auto'
                });
                $('*[data-tickid="' + tickid + '"]').html('Follow');
            }, 400);
            delete $scope.userfollowteam[teamid];
            $scope.$apply();
            console.log(teamid);
            console.log($scope.userfollowteam);

        }, 400);

    }

    $scope.unfollowteam = function (teamid, event) {

        console.log($scope.userfollowteam[teamid]);


        deletebyteamanduseridsuccess = function (response) {
            console.log(response);
            //SET FOLLOWING VALUE OF TEAM AS FALSE HERE
            unfollowanimation(teamid, event);

        }
        deletebyteamanduseriderror = function (error) {
            console.log(error);
        }



        ApiServices.deletedatabyid('User_followteam', $scope.userfollowteam[teamid].id).then(deletebyteamanduseridsuccess, deletebyteamanduseriderror);
    };



    /*GET THE ALL TEAMS FROM TABLE TO FOLLOW*/
    //        ApiServices.getteamstofollow().then(getalldatasuccess, getalldataerror);

    getmanybysuccess = function (response) {
        console.log(response);
        _.forEach(response.data, function (userfollow, key) {
            for (var cat in $rootScope.dataforfixture) {
                for (group in $rootScope.dataforfixture[cat].groups) {
                    var index = $rootScope.dataforfixture[cat].groups[group].teams.findIndex(team => team.id == userfollow.team_id);

                    if (index != -1) {
                        console.log(userfollow.team_id);
                        setTimeout(function () {
                            $('#tick' + userfollow.team_id).show();
                            $('#tick' + userfollow.team_id).css({
                                'height': '35px',
                                'width': '35px',
                                'transform': 'rotateZ(360deg)',
                                'opacity': '1'
                            })
                        }, 0);


                    }

                    $scope.userfollowteam[userfollow.team_id] = userfollow;


                }
            }
        });
        console.log($scope.userfollowteam);
    }

    getmanybyerror = function (error) {
        console.log(error);
    }



    ApiServices.getmanyby('User_followteam', 'user_id', $.jStorage.get('fcbuserdata').id).then(getmanybysuccess, getmanybyerror);
})

.controller('photosCtrl', function ($scope, $rootScope, $location, $interval, $ionicLoading, $stateParams, ApiServices, $ionicPopup) {
        /*INITIALIZATIONS*/
        var albumid;
        $scope.photos = [];
        //         $scope.$on('$ionicView.enter', function (e) {
        //         })
        albumid = $stateParams.albumid;
        /*LOCAL FUNCTIONS */
        /*var myPopup = $ionicPopup.show({
            templateUrl: './templates/photopopup.html',
            scope: $scope,
            cssClass: 'photopopup'

        });*/


        /*SCOPE FUNCTIONS*/


        console.log(albumid);

        /*CALLBACks*/

        getalldatasuccess = function (response) {
            console.log(response);
            $scope.photos = response.data;
            slickinitialize();
        }

        getalldataerror = function (error) {
            console.log(error);
        }




        ApiServices.getmanyby('photos', 'album_id', albumid).then(getalldatasuccess, getalldataerror);

        /*Image Slide Starts Here	*/

        /*
        	variables
        */

        var $imagesSlider = $(".gallery-slider__images>div "),
            $thumbnailsSlider = $(".gallery-slider__thumbnails>div");
        console.log($imagesSlider);


        //        	sliders

        var slickinitialize = function () {
            setTimeout(function () {
                /*Image Slide Ends Here	*/
                $imagesSlider.slick({
                    speed: 300,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    cssEase: 'linear',
                    fade: true,
                    draggable: false,
                    asNavFor: ".gallery-slider__thumbnails>div",
                    prevArrow: '.gallery-slider__images .prev-arrow',
                    nextArrow: '.gallery-slider__images .next-arrow'
                });

                // thumbnails options
                $thumbnailsSlider.slick({
                    speed: 300,
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    cssEase: 'linear',
                    centerMode: true,
                    draggable: false,
                    focusOnSelect: true,
                    asNavFor: ".gallery-slider .gallery-slider__images>div",
                    prevArrow: '.gallery-slider__thumbnails .prev-arrow',
                    nextArrow: '.gallery-slider__thumbnails .next-arrow',
                    responsive: [
                        {
                            breakpoint: 720,
                            settings: {
                                slidesToShow: 4,
                                slidesToScroll: 4
                            }
    },
                        {
                            breakpoint: 576,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 3
                            }
    },
                        {
                            breakpoint: 350,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2
                            }
    }
   ]
                });
            }, 500)


            $imagesSlider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                $('.main-image img').addClass('wow zoomIn animated');
            });

            // update the caption after the image is changed
            $imagesSlider.on('afterChange', function (event, slick, currentSlide, nextSlide) {
                $('.main-image img').removeClass('wow zoomIn animated');

            });

            /*     var $caption = $('.gallery-slider .caption');

            // get the initial caption text
            var captionText = $('.gallery-slider__images .slick-current img').attr('alt');
            updateCaption(captionText);

            // hide the caption before the image is changed
            $imagesSlider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                $caption.addClass('hide');
            });

            // update the caption after the image is changed
            $imagesSlider.on('afterChange', function (event, slick, currentSlide, nextSlide) {
                captionText = $('.gallery-slider__images .slick-current img').attr('alt');
                updateCaption(captionText);
            });

            function updateCaption(text) {
                // if empty, add a no breaking space
                if (text === '') {
                    text = '&nbsp;';
                }
                $caption.html(text);
                $caption.removeClass('hide');
            }

*/
        }

        // images options

        //         
        //        	captions
        //        


    })
    //followingCtrl
    .controller('followingCtrl', function ($scope, $rootScope, $location, $interval, $ionicLoading, $stateParams, ApiServices) {



        /*CallBacks*/
        getfixturesoffollowingteamsuccess = function (response) {
            console.log(response);
            $scope.followingteamsinformation = response.data;

        }

        getfixturesoffollowingteamerror = function (error) {
            console.log(error);
        }






        $scope.$on('$ionicView.enter', function () {
            $scope.followingteamsinformation = [];



            ApiServices.getfixturesoffollowingteam().then(getfixturesoffollowingteamsuccess, getfixturesoffollowingteamerror);
        });


    })