var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCss = require('gulp-clean-css');
var rename = require('gulp-rename');
var minify = require('gulp-minify');
var uglify = require('gulp-uglify');

var paths = {
    sass: ['./www/scss/*.scss']
};

gulp.task('default', ['sass']);

gulp.task('sass', function (done) {
    console.log('called');
    gulp.src('./www/scss/ionic.app.scss')
        .pipe(sass())
        .on('error', sass.logError)
        .pipe(gulp.dest('./www/css/'))
        .pipe(cleanCss({
            keepSpecialComments: 0
        }))
        .pipe(rename({
            extname: '.min.css'
        }))
        .pipe(gulp.dest('./www/css/'))
        .on('end', done);
});


gulp.task('minifyjs', function () {
    gulp.src('./www/js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('./www/js/*.js'))
});

//gulp.task('watch', ['sass'], function() {
gulp.watch(paths.sass, ['sass']);
//});