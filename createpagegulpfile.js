var gulp = require('gulp');
var guidePageGenerator = require('gulp-guide-page-generator');
var parameterized = require('gulp-parameterized');
inject = require('gulp-inject-string');

gulp.task('g', parameterized(function (cb, params) {
    console.log(params);
    gulp.src('www/templates')
        .pipe(guidePageGenerator({
            title: params.name + 'Page',
            template: 'www/templates/search.html',
            filename: params.name + '.html'
        }))
        .pipe(gulp.dest('www/templates'));


    gulp.src('www/scss/layouts')
        .pipe(guidePageGenerator({
            template: '',
            filename: '_' + params.name + '.scss'
        }))
        .pipe(gulp.dest('www/scss/layouts'));


    injectintofile(params);
}));

gulp.task('default', function () {
    console.log('I am aslo called');
})


injectintofile = function (params) {
    gulp.src('www/js/app.js')
        .pipe(inject.before('.state', `\n.state('app.` + params.name + `', {
                url: '/` + params.name + `',
                views:{
 'menuContent': {
                templateUrl: 'templates/` + params.name + `.html',
                controller: '` + params.name + `Ctrl',
            }}})\n`))

        .pipe(gulp.dest('www/js'));
    gulp.src('www/js/controllers.js')
        .pipe(inject.append(`\n  .controller('` + params.name + `Ctrl', function ($scope, ApiServices, $rootScope, $location, $interval, $ionicLoading,  $stateParams) {})`))

        .pipe(gulp.dest('www/js'));
    gulp.src('www/scss/_layouts.scss')
        .pipe(inject.append(`\n  @import "layouts/_` + params.name + '.scss";'))

        .pipe(gulp.dest('www/scss'));
};
