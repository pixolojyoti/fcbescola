var apiservices = angular.module('apiservices', [])

    .factory('ApiServices', function ($http, $ionicPopup, $cordovaToast) {

        //MACBOOK AND HOME LAPTOP
        // var adminurl = "http://localhost/rest/rest/index.php/";
        //PC
        //var adminurl = "http://localhost/inqrest/rest/index.php/";
        //SERVER
        var adminurl = "http://pixoloproductions.com/fcb/fcbescolarest/index.php/";

        //FACEBOOK API
        var facebookgraphurl = "https://graph.facebook.com/v3.0/";
        //HOME LAPTOP

        var getformdata = function (objectdata) {
            var formdata = new FormData();
            for (var key in objectdata) {
                formdata.append(key, objectdata[key]);
            }
            return formdata;
        }




        return {



            showtoast: function (msg, size, pos) {

                $cordovaToast.show(msg, size, pos).then(function (success) {
                    // success
                    console.log(success);
                }, function (error) {
                    // error
                    console.log(error);
                });



            },
            showloadingpopup: function () {
                console.log('showloading popup');
                var myPopup = $ionicPopup.show({
                    template: '<div class="post-loader"><ion-spinner icon="android"><ion-spinner class="spinner-energized"></ion-spinner></ion-spinner><p>Loading...</p></div>',
                    cssClass: 'loading-popup',

                });
                return myPopup;

            },



            showpopup: function (title, msg) {
                // SHOW ERROR FUNCTION TO COME HERE
                console.log(title);
                var myPopup = $ionicPopup.show({

                    title: title,
                    template: msg,
                    cssClass: 'errorpopup',
                    buttons: [
                        {
                            text: 'Ok',
                            type: 'button-default'
                        }]
                });

                
            },
            uploadimage: function (imagedata) {
                console.log(imagedata);
                var formdata = getformdata(imagedata);
                return $http({
                    url: adminurl + 'teams/uploadImage',
                    method: "POST",
                    headers: {
                        'Content-Type': undefined,
                    },
                    data: formdata,
                    transformRequest: angular.identity
                });
            },
            checkbeforeloggedin: function (userdata) {
                var formdata = getformdata(userdata);
                return $http({
                    url: adminurl + 'Users/checkuserandadd',
                    method: "POST",
                    headers: {
                        'Content-Type': undefined,
                    },
                    data: formdata,
                    transformRequest: angular.identity
                });
            },
            deleteimage: function (imagedata) {
                console.log(imagedata);
                var formdata = getformdata(imagedata);
                return $http({
                    url: adminurl + 'teams/deleteImage',
                    method: "POST",
                    headers: {
                        'Content-Type': undefined,
                    },
                    data: formdata,
                    transformRequest: angular.identity
                });
            },
            getalldata: function (controllername) {
                console.log('called');
                return $http({
                    url: adminurl + controllername + '/getall',
                    method: "GET",
                })
            },
            editdata: function (controllername, datatobeedited) {
                console.log(datatobeedited);
                var formdata = getformdata(datatobeedited);
                console.log(formdata.get('id'))
                return $http({
                    url: adminurl + controllername + '/postupdatebyid',
                    method: 'POST',
                    headers: {
                        'Content-Type': undefined
                    },
                    data: formdata,
                    transformRequest: angular.identity

                })

            },
            storepostdata: function (controllername, tournament) {
                var formdata = getformdata({
                    "data": angular.toJson(tournament)
                });
                return $http({
                    url: adminurl + controllername + '/postinsert',
                    method: 'POST',
                    headers: {
                        'Content-Type': undefined
                    },
                    data: formdata,
                    transformRequest: angular.identity

                })
            },
            deletedatabyid: function (controllername, id) {
                return $http.get(adminurl + controllername + '/deletebyid', {
                    params: {
                        id: id
                    }
                });

            },

            getdatabyid: function (controllername, id) {
                return $http.get(adminurl + controllername + '/getbyid', {
                    params: {
                        id: id
                    }
                });

            },
            getdataforfixtures: function () {

                return $http.get(adminurl + 'tournaments/gettournamentinfodata', {
                    params: {
                        tournament_id: 24
                    }
                });
            },
            getfixturesbytournamentid: function (tournamentid) {
                return $http.get(adminurl + 'fixtures/getallfixturesbytournamentid', {
                    params: {
                        tournament_id: tournamentid
                    }
                });
            },
            submitresult: function (result) {
                return $http.get(adminurl + 'results/updateresult', {
                    params: result
                });
            },
            getmanyby: function (controllername, field, value) {
                return $http.get(adminurl + controllername + '/getmanyby', {
                    params: {
                        field: field,
                        value: value
                    }
                });

            },
            getteams: function () {
                //

                return $http.get(adminurl + 'teams/getteamsforgroup', {
                    params: {
                        //                        tournament_id:$.jStorage.get("tournament");
                        tournament_id: 24
                    }
                });

            },
            getgroupteam: function (groupid) {
                return $http.get(adminurl + 'groups/getGroupTeams', {
                    params: {
                        group_id: groupid
                    }
                });
            },


            // FIXTURE CREATION API's 
            deletefixturebyid: function (fixtureid) {
                return $http.get(adminurl + 'fixtures/deletebyid', {
                    params: {
                        id: fixtureid
                    }
                });
            },
            createfixture: function (fixtureobj) {
                var formdata = getformdata({
                    "data": angular.toJson(fixtureobj)
                });
                return $http({
                    url: adminurl + controllername + '/postinsert',
                    method: 'POST',
                    headers: {
                        'Content-Type': undefined
                    },
                    data: formdata,
                    transformRequest: angular.identity

                })
            },
            getteamstofollow: function () {
                return $http.get(adminurl + 'teams/getall', {
                    params: {
                        id: $.jStorage.get('fcbuserdata').id
                    }
                });
            },
            deletebyteamanduserid: function (teamid) {
                return $http.get(adminurl + 'user_follow/deleteuserfollowing', {
                    params: {
                        team_id: teamid,
                        user_id: $.jStorage.get('fcbuserdata').id
                    }
                });

            },
            getfacebookuserdetails: function (access_token) {
                return $http.get(facebookgraphurl + "me", {
                    params: {
                        access_token: access_token,
                        debug: "all",
                        fields: "id,name,email,picture",
                        format: "json",
                        method: "get",
                        pretty: "0",
                        suppress_http_code: "1"

                    }
                });
            },

            getlikedpages: function (access_token) {
                return $http.get(facebookgraphurl + "me/likes", {
                    params: {
                        access_token: access_token,
                        pretty: "0",
                        limit: "1000"
                    }
                });
            },

            getalbum: function () {
                return $http({
                    url: adminurl + 'albums/getalbumpagedata',
                    method: "GET",
                })


            },
            getallstories: function () {

                return $http({
                    url: adminurl + 'Stories/getstories',
                    method: "GET",
                });


            },
            getdataforpointstable: function (categoryid) {

                return $http({
                    url: adminurl + 'groups/getpointstablefromcategoryid',
                    method: "GET",
                    params: {
                        category_id: categoryid
                    }


                });

            },
            getfixturesoffollowingteam: function () {

                return $http({
                    url: adminurl + 'Fixtures/getfixturesoffollowingteams',
                    method: "GET",
                    params: {
                        user_id: $.jStorage.get('fcbuserdata').id
                    }


                });


            },
            reportstory: function (postid) {

                return $http.get(adminurl + 'stories/reportstorybyid', {
                    params: {
                        story_id: postid
                    }
                });


            },
            updatefacebookprofileimage: function (facebookdata) {
                console.log(facebookdata);
                var formdata = getformdata(facebookdata);
                return $http({
                    url: adminurl + 'Users/updatefacebookprofileimage',
                    method: "POST",
                    headers: {
                        'Content-Type': undefined,
                    },
                    data: formdata,
                    transformRequest: angular.identity
                });

            },
            viewanalyse: function (view) {
                if (window.location.hostname != "localhost") {
                    if (typeof analytics !== 'undefined') {
                        analytics.trackView(view);
                    }
                }
            },




        }
    });
