var imageurl = "http://pixoloproductions.com/fcb/fcbescolarest/uploads/";
angular.module('starter.controllers', [])

    .controller('AppCtrl', function ($scope, $ionicModal, $timeout, $location, $rootScope, ApiServices, $ionicSideMenuDelegate, $state, $cordovaNetwork, $filter, $ionicPopup) {

        // With the new view caching in Ionic, Controllers are only called
        // when they are recreated or on app start, instead of every page change.
        // To listen for when this page is active (for example, to refresh data),
        // listen for the $ionicView.enter event:

        /*CHECK FOR INTERNET CONNECTION*/




        /*INTIALIAZATION*/

        $rootScope.teamimages = [];
        document.addEventListener("deviceready", function () {
            $scope.offline = $cordovaNetwork.isOffline();

        }, false);
        //        document.addEventListener("offline", onOffline, false);
        //
        //
        //        function onOffline() {
        //            // Handle the offline event
        //            console.log('fires');
        //
        //
        //            var popup = ApiServices.showpopup('Offline', 'You are offline ! Please check your internet connection  !');
        //
        //
        //        }
        //        document.addEventListener("online", onOnline, false);
        //
        //        function onOnline() {
        //
        //            $location.path($location.path());
        //            $rootScope.$apply()
        //            // Handle the offline event
        //
        //
        //        }



        $rootScope.$on('$cordovaNetwork:online', function (event, networkState) {
            console.log(online, $cordovaNetwork.isOffline());
            $scope.offline = $cordovaNetwork.isOffline();
            $location.path($location.path());
            $scope.$apply()


        });


        $rootScope.$on('$cordovaNetwork:offline', function (event, networkState) {
            console.log('offline', $cordovaNetwork.isOffline());
            if ($scope.offline) {

                if ($scope.offline) {
                    var popup = ApiServices.showpopup('Offline', 'You are offline ! Please check your internet connection  !');
                    $scope.offline = false;

                };
            }



        });






        /*Get Profile Image and change*/



        $scope.$on('$ionicView.enter', function (e) {
            console.log('App Ctrl');
            var path = $location.path();

            $rootScope.showtabs = (path != '/app/login');
            console.log($rootScope.showtabs);

            var location = $location.path();

            ApiServices.viewanalyse(location);

            console.log($state);

            if (location == "/app/login") {
                console.log("MAKE FALSEE");
                $rootScope.isloggedin = false;
                $ionicSideMenuDelegate.canDragContent(false);
            } else {
                $rootScope.isloggedin = $.jStorage.get('fcbuserdata') ? $.jStorage.get('fcbuserdata').facebook_access_token == '' : true;

                $scope.activetab = $state.current.url.substr(1);
                console.log($scope.activetab);
                console.log("MAKE TRUE");
                $ionicSideMenuDelegate.canDragContent(true);
            };

        });



        var toDataURL = function (url, i, g, t, image) {

            var xhr = new XMLHttpRequest();

            xhr.onload = function () {
                var reader = new FileReader();
                reader.onloadend = function () {

                    $rootScope.teamimages[$rootScope.dataforfixture[i].groups[g].teams[t].id] = {};
                    $rootScope.teamimages[$rootScope.dataforfixture[i].groups[g].teams[t].id].image = image;
                    $rootScope.teamimages[$rootScope.dataforfixture[i].groups[g].teams[t].id].base = reader.result;
                    $.jStorage.set("teamimages", $rootScope.teamimages)
                };
                reader.readAsDataURL(xhr.response);
            };

            xhr.open('GET', url);
            xhr.responseType = 'blob';
            xhr.send();

        };



        if ($.jStorage.get("teamimages") == null) {
            $.jStorage.set("teamimages", []);
        };



        /*GET INFORMATIONS FOR FIXTURES */

        $rootScope.fcbuserdata = $.jStorage.get('fcbuserdata');

        var preparepopup;

        getdataforfixturessuccess = function (response) {
            console.log(response);
            $rootScope.dataforfixture = response.data;
            $.jStorage.set('dataforfixture', response.data);

            preparepopup = $ionicPopup.show({
                title: "#VamosLetsPlay",
                template: "Please wait while we are preparing your playground.",
                cssClass: 'errorpopup'
            });
            for (var i = 0; i < $rootScope.dataforfixture.length; i++) {
                for (var g = 0; g < $rootScope.dataforfixture[i].groups.length; g++) {
                    for (var t = 0; t < $rootScope.dataforfixture[i].groups[g].teams.length; t++) {

                        $rootScope.teamimages[$rootScope.dataforfixture[i].groups[g].teams[t].id] = new Image();

                        $rootScope.teamimages[$rootScope.dataforfixture[i].groups[g].teams[t].id].src = $filter('imagepath')($rootScope.dataforfixture[i].groups[g].teams[t].image, "teams");

                        //toDataURL($filter('imagepath')($rootScope.dataforfixture[i].groups[g].teams[t].image, "teams"), i, g, t, $rootScope.dataforfixture[i].groups[g].teams[t].image);

                    }
                }
            };

            preparepopup.close();


        };

        getdataforfixtureserror = function (error) {
            console.log(error);
        };



        ApiServices.getdataforfixtures().then(getdataforfixturessuccess, getdataforfixtureserror);


        $rootScope.dataforfixture = $.jStorage.get("dataforfixture");


        // SET USER DATA IN ROOT SCOPE
        if ($.jStorage.get("fcbuserdata") != undefined) {
            // SET USER DATA IN ROOT SCOPE
            $rootScope.fcbuserdata = $.jStorage.get("fcbuserdata");

        } else {
            $rootScope.fcbuserdata = {
                name: "FCB User",
                facebook_profile_photo: "http://pixoloproductions.com/fcb/fcb_default_profile.svg",
                facebook_access_token: ""
            };
        };


        $scope.logout = function () {
            $.jStorage.deleteKey('fcbuserdata');
            $location.path('app/login');
        }
    })




    .controller('albumCtrl', function ($scope, $rootScope, $location, $interval, $ionicLoading, $stateParams, ApiServices, $ionicPopup) {
        /*INITIALIZATIONS*/
        $scope.albums = [];
        var loadingpopup = ApiServices.showloadingpopup();

        getalbumsuccess = function (response) {
            console.log(response);
            loadingpopup.close();
            $scope.albums = response.data;
        }
        getalbumerror = function (error) {
            loadingpopup.close();
            console.log(error);
        }




        /*SLIDER ATTRIBUTE SETTINGS*/

        $scope.$on("$ionicSlides.sliderInitialized", function (event, data) {
            console.log('data', data);
            // grab an instance of the slider
            $scope.slider = data.slider;
            $scope.path = 'gallery';
        });

        function dataChangeHandler() {
            // call this function when data changes, such as an HTTP request, etc
            if ($scope.slider) {
                $scope.slider.updateLoop();
            }
        }

        /*------------------------------------------*/

        /*Scope Functions*/
        $scope.getphotos = function (album) {
            console.log('Called');

            $scope.photos = []
            $scope.options = {
                loop: false,
                effect: 'slide',
                speed: 500,
                pagination: false
            };

            getalldatasuccess = function (response) {
                console.log(response.data);

                $scope.photos = response.data;


                /* OPEN POPUP */
                var photoPopup = $ionicPopup.show({
                    cssClass: 'photo-popup',
                    templateUrl: 'templates/photopopup.html',
                    scope: $scope
                });

                //                $scope.$apply();
                setTimeout(function () {

                    $('.photo-popup').click(function () {
                        photoPopup.close();
                    });


                }, 0);


            };

            getalldataerror = function (error) {
                console.log(error);
            };




            ApiServices.getmanyby('photos', 'album_id', album.id).then(getalldatasuccess, getalldataerror);




        }


        ApiServices.getalbum().then(getalbumsuccess, getalbumerror);

    })

    .controller('fixturesCtrl', function ($scope, $rootScope, $location, $interval, $ionicLoading, $stateParams, ApiServices) {

        /*INITIALIZATION*/
        var loadingpopup = ApiServices.showloadingpopup();
        var tournamentid = '24';
        $scope.fixtures = [];
        var todaysdate = new Date().getTime();
        var categoryid = $stateParams.catid;
        var scrolltoindex = -1;



        // GET FIXTURES BY TOURNAMENT ID
        getfixturesbytournamentidsuccess = function (response) {
            //$scope.fixtures = response.data;
            loadingpopup.close();
            console.log(response.data);

            var lastdate = "";
            var lastcategoryid = "";

            var datearray = [];
            $scope.datesortedfixtures = [];

            $scope.sortedarray = [];

            console.log(response.data);

            response.data.forEach(function (value, key) {
                if (value.category_id == categoryid) {





                    if (lastdate != value.date) {
                        $scope.sortedarray.push({
                            date: value.date,
                            categories: []
                        });
                        lastdate = value.date;
                        var matchdate = new Date(value.matchdate).getTime();
                        if (scrolltoindex == -1) {
                            console.log(matchdate, todaysdate);
                            if (todaysdate <= (matchdate + 86400000)) {
                                scrolltoindex = $scope.sortedarray.length - 1;
                            };
                        };
                        lastcategoryid = "";
                    };

                    if (lastcategoryid != value.category_id) {
                        $scope.sortedarray[$scope.sortedarray.length - 1].categories.push({
                            category: value.category_name,
                            category_age: value.category_age,
                            fixtures: []
                        });

                        lastcategoryid = value.category_id;
                    };


                    sl = $scope.sortedarray.length - 1;
                    cl = $scope.sortedarray[$scope.sortedarray.length - 1].categories.length - 1;

                    $scope.sortedarray[sl].categories[cl].fixtures.push(value);

                    /* $scope.fixtures = response.data;
                     console.log($scope.fixtures);
                     $scope.fixtures.forEach(function (value, key) {

                     });*/

                    console.log($scope.sortedarray);

                    //setdateslider();

                    //                    /*console.log($scope.sortedarray)

                    /*response.data.forEach(function (value, key) {


                        if (lastdate != value.date) {

                            if (datearray.length > 0) {
                                $scope.datesortedfixtures.push(datearray);
                            };

                            lastdate = value.date;

                            datearray = [];

                        };

                        datearray.push(value);


                    });*/

                    //                    console.log($scope.datesortedfixtures);
                    var catarray = [];
                    $scope.catsortedarray = [];
                    var lastcat = "";


                    /*$scope.datesortedfixtures.forEach(function (value, key) {
                        value.forEach(function (value2, key) {
                            if (lastcat != value2.category_id) {
                                if (catarray.length > 0) {
                                    $scope.catsortedarray.push(catarray);
                                };
                                lastcat = value2.category_id;
                                catarray = [];
                            };
                            catarray.push(value2);
                        });
                    });*/
                }

            });


            setTimeout(function () {
                scrolltodate(scrolltoindex);
            }, 0);

        };
        getfixturesbytournamentiderror = function (error) {
            loadingpopup.close();
            console.log(error);
        };


        $scope.$on('$ionicView.enter', function () {
            ApiServices.getfixturesbytournamentid(tournamentid).then(getfixturesbytournamentidsuccess, getfixturesbytournamentiderror);
        })




        /* FIXTURES EFFECT */

        var children;

        var setdateslider = function () {
            setTimeout(function () {
                children = document.getElementById('dates-slider').children;
                changedatesliderpositions(0);

                /*
                            CHECK DIV ON SCROLL 
                
                            $('.scroll-content').scroll(function () {
                                $("#full-date-fixture-item .date-wrapper").each(function () {
                                    console.log($(this).offset().top);
                                    console.log($('.scroll-content').scrollTop());
                                    if (!autoscrolling) {
                                        if ($(this).offset().top < 80) {
                                            var idstring = $(this).attr('id');
                                            var newind = parseInt(idstring.substring(5, idstring.length));
                                            changedatesliderpositions(newind);
                                        };
                                    };
                                });
                            });
                
                            */
            }, 0);
        };

        var scrolltodate = function (ind) {
            $('.scroll-content').animate({
                scrollTop: ($(".scroll-content").scrollTop() + ($("#date-" + ind).offset().top - 65))
            }, 2000);
            return true;
        };

        var removeallclassess = function (el) {
            el.classList.remove("active", "prev", "next", "out-left", "out-right");
        };

        var changedatesliderpositions = function (ind) {

            for (var ds = 0; ds < children.length; ds++) {

                //DONT GO THROUGH LOOP
                if (ds == (ind + 1)) {
                    continue;
                };

                //MAKE ACTIVE AND NEXT
                if (ds == ind) {
                    removeallclassess(children[ds]);
                    children[ds].classList.add('active');
                    if (ds + 1 < children.length) {
                        removeallclassess(children[ds + 1]);
                        children[ds + 1].classList.add('next');
                    };
                };

                // SET PREVIOUS
                if ((ds + 1) == ind) {
                    removeallclassess(children[ds]);
                    children[ds].classList.add('prev');
                };

                // SEND TO LEFT
                if (ds < (ind - 1)) {
                    removeallclassess(children[ds]);
                    children[ds].classList.add('out-left');
                };

                // SEND TO RIGHT
                if (ds > ind) {
                    removeallclassess(children[ds]);
                    children[ds].classList.add('out-right');
                };



            };
            return true;
        };



        var autoscrolling = false;
        $scope.changeSlider = function (ind) {
            console.log("CLICKED");
            autoscrolling = true;
            changedatesliderpositions(ind);
            scrolltodate(ind);
            autoscrolling = false;
        };


    })
    .controller('loginCtrl', function ($scope, $rootScope, $location, $interval, $ionicLoading, $stateParams, ApiServices, $cordovaDevice, $cordovaOauth) {
        var userdata = {};
        var data = {};
        $scope.$on('$ionicView.enter', function () {
            /*INITIALIZATION*/
            data = {}; //       


            userdata = {
                name: "",
                email: "",
                password: "",
                device_id: "",
                facebook_access_token: "",
                facebook_id: "",
                facebook_profile_photo: "http://pixoloproductions.com/fcb/fcb_default_profile.svg",
                onesignal_deviceid: ''
            };

            document.addEventListener("deviceready", function () {
                // GET DEVICE ID OF PHONE FROM CORDOVA
                if (window.location.host == "localhost:8100") {
                    userdata.device_id = "device";
                } else {
                    userdata.device_id = $cordovaDevice.getUUID();



                    window.plugins.OneSignal.getIds(function (ids) {
                        userdata.onesignal_deviceid = JSON.stringify(ids['userId']);
                    });
                };
            }, false);
            /* setTimeout(function () {
                
                window.plugins.OneSignal.getIds(function (ids) {
                    userdata.onesignal_deviceid = JSON.stringify(ids['userId']);
                });
            }, 0);
*/
        });









        /*CALLBACK*/

        // STORE THE USERS DATA IN DATABASE
        var checkbeforeloggedinsuccess = function (response) {
            console.log(response.data);
            userdata.id = response.data.id;
            if (userdata.name == '') {
                userdata.name = 'Unknown'
            }
            console.log(userdata);
            // STORE FCBUSERDATA IN JSTORAGE
            $.jStorage.set("fcbuserdata", userdata);

            // STORE IN ROOT SCOPE FOR APP USE
            $rootScope.fcbuserdata = userdata;

            // REDIRECT TO TIMELINE SINCE FACEBOOK LOGIN IS COMPLETE
            $location.path('/app/timeline');
        };
        var checkbeforeloggedinerror = function (response) {
            console.log(response.data);

        };


        //        userdata.device_id = $cordovaDevice.getUUID();
        $scope.facebooklogin = function () {
            $cordovaOauth.facebook("1968132213517198", ["email", 'user_likes', 'user_photos', 'user_posts', 'user_events']).then(function (result) {
                // USER LOGGED IN TO FACEBOOK SUCCESSFULLY
                console.log(result); //                $scope.data = result.access_token;
                userdata.facebook_access_token = result.access_token;



                // GET ALL INFORMATION FROM FACEBOOK USING THE ACCESS TOKEN
                var getfacebookuserdetailssuccess = function (response) {

                    userdata.name = response.data.name;
                    userdata.email = response.data.email;
                    userdata.facebook_id = response.data.id;
                    userdata.facebook_profile_photo = "http://graph.facebook.com/" + response.data.id + "/picture?width=50&height=50";
                    console.log(userdata);
                    //CHECK IF USER IS LOGGED IN VIA FACEBOOK BEFORE OR NOT
                    ApiServices.checkbeforeloggedin(userdata).then(checkbeforeloggedinsuccess, checkbeforeloggedinerror);
                    //                    ApiServices.storepostdata('users', userdata).then(storepostdatasuccess, storepostdataerror);


                };
                var getfacebookuserdetailserror = function (response) {
                    console.log(response.data);
                    // INTERNET ERROR
                };
                ApiServices.getfacebookuserdetails(result.access_token).then(getfacebookuserdetailssuccess, getfacebookuserdetailserror);



            }, function (error) {
                alert("Error : " + error);
            });
        };


        //FACEBOOK LOGIN SKIPPED FUNCTION
        $scope.loginskip = function () {
            ApiServices.checkbeforeloggedin(userdata).then(checkbeforeloggedinsuccess, checkbeforeloggedinerror);
            $location.path('/app/fixtures');
        }


        storepostdatasuccess = function (response) {
            console.log(response);
            userdata.id = response.data;
            $.jStorage.set('fcbuserdata', userdata);

        }
        storepostdataerror = function (error) {
            console.log(error);
        }




    })
    .controller('pointstableCtrl', function ($scope, $rootScope, $location, $interval, $ionicLoading, $stateParams, ApiServices) {

    })
    .controller('addstoryCtrl', function ($scope, $rootScope, $location, $interval, $ionicLoading, $stateParams, ApiServices, $ionicPlatform, $cordovaCamera, $cordovaSocialSharing) {
        console.log(imageurl);

        /*INITIALIZATION*/
        $scope.post = {
            caption: '',
            image: '',
            video: '',
            user_id: $.jStorage.get('fcbuserdata').id,

        };


        $scope.sensoredwords = [];
        $scope.fileURI = '';
        var pictureSource; // picture source
        var destinationType; // sets the format of returned value
        $scope.toggle = {};

        //            FileTransfer Object
        var ft;


        function onDeviceReady() {
            console.log('device ready');
            pictureSource = navigator.camera.PictureSourceType;
            destinationType = navigator.camera.DestinationType;
            ft = new FileTransfer();
        };



        /* var findsensoredwords = function () {



         };*/



        getalldatasuccess = function (response) {
            console.log(response);
            $scope.sensoredwords = response.data;
        }
        getalldataerror = function (error) {
            console.log(error);
        }


        /*Get all Sensored words*/



        ApiServices.getalldata('Cusswords').then(getalldatasuccess, getalldataerror);

        /*SHARE ON FACEBOOK*/
        var sharepostonfacebook = function () {
            console.log(ionic.Platform.device());
            if ($scope.toggle.value) {

                if ($scope.post.caption != '' || $scope.fileURI != '') {
                    console.log($scope.post.caption);
                    $cordovaSocialSharing.shareViaFacebook($scope.post.caption, $scope.fileURI, "", $scope.post.caption)
                        .then(function (result) {
                            // Success!
                            console.log(result);
                        }, function (err) {
                            ApiServices.showtoast('Facebook should be installed in you phone to share this post', 'long', 'bottom');
                            // An error occurred. Show a message to the user
                            console.log(err);
                        });

                } else {
                    ApiServices.showtoast("Can't post empty message", 'short', 'bottom');
                }




            }
            $scope.fileURI = '';
            postloading.close();
            $location.path('app/timeline');

        };
        document.addEventListener("deviceready", onDeviceReady, false);


        var submitpost = function () {
            console.log('postdata', $scope.post);

            storepostdatasuccess = function (response) {
                console.log(response);
                sharepostonfacebook();
            }

            storepostdataerror = function (error) {
                console.log(error);
            }


            /*CHECK IF POST CONTAINS SENSORED WORDS*/
            var wordfound = false;
            console.log($scope.sensoredwords.length);
            for (var index in $scope.sensoredwords) {

                console.log(index);
                console.log(new RegExp($scope.sensoredwords[index].word));
                var word = $scope.post.caption.match(new RegExp($scope.sensoredwords[index].word, "i"));
                if (word) {

                    console.log('In if ', word);
                    wordfound = true;
                    break;
                }

            };
            if (!wordfound) {
                ApiServices.storepostdata('stories', $scope.post).then(storepostdatasuccess, storepostdataerror);
            } else {

                //            SHOW POPUP HERE FOR USING BAD WORDS
                ApiServices.showpopup("Abusive Content", "You may want to rephrase it.");
                console.log('Word found');
            }
            //           

        };

        function clearCache() {
            navigator.camera.cleanup();
        }

        var onCapturePhoto = function (fileURI) {
            console.log(fileURI);
            $scope.fileURI = fileURI;

            /*CALL TO OTHER FUNCTION*/
            $scope.imageuploaded = true;
            $scope.showimg = true;

            var image = document.getElementById('mypostimage');
            console.log(image);
            if (image != null) {

                image.src = $scope.fileURI;
                $scope.$apply();
            };

            clearCache();

        };
        var onFail = function (message) {
            alert('Failed because: ' + message);
        };
        /*PHOTOS UPLOAD FROM CAMERA*/
        $scope.photouploadfromcamera = function () {

            /*FOR ANDROID */
            if (ionic.Platform.isAndroid()) {
                console.log(destinationType);
                navigator.camera.getPicture(onCapturePhoto, onFail, {
                    quality: 30,
                    destinationType: destinationType.FILE_URI,
                    allowEdit: true,
                    correctOrientation: true
                });

            }

        };







        /*PHOTOS UPLOAD FROM GALLERY*/
        $scope.photouploadfromgallery = function () {
            /*FOR ANDROID */
            if (ionic.Platform.isAndroid()) {
                navigator.camera.getPicture(onCapturePhoto, onFail, {
                    quality: 30,
                    destinationType: destinationType.FILE_URI,
                    sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                    allowEdit: true,
                    correctOrientation: true
                });
            }
        };


        //    CANCEL POSTING
        $scope.cancelposting = function () {
            $location.path('app/timeline');
        }
        $scope.removeimage = function () {
            $scope.fileURI = '';
            var image = document.getElementById('mypostimage');
            image.src = '';
            $scope.$apply();

        }

        var postloading;

        //    SUBMIT POST
        $scope.transferfile = function () {


            postloading = ApiServices.showloadingpopup();


            /*IF IMAGE IS UPLOADED*/
            if ($scope.fileURI != '') {
                console.log('transferfile', ft);
                // UPLOAD FILE SUCCESS
                var win = function (data, status) {

                    console.log('data', data);
                    $scope.post.image = data.response;
                    submitpost();
                };
                //UPLOAD FILE FAILURE
                var fail = function (error) {
                    console.log('error', error);
                };

                console.log($scope.fileURI);

                $scope.imgname = angular.copy($scope.fileURI.substr($scope.fileURI.lastIndexOf('/') + 1));

                if ($scope.imgname.lastIndexOf('?') != -1) {
                    $scope.imgname = $scope.imgname.substring(0, $scope.imgname.lastIndexOf('?'));
                };
                var options = new FileUploadOptions();
                options.fileKey = "image";
                options.fileName = $scope.imgname;
                options.mimeType = "image/jpeg";

                var params = {};
                params.value1 = "test";
                params.value2 = "param";

                options.params = params;
                options.chunkedMode = false;

                console.log($scope.fileURI);
                console.log(options);
                ft.upload($scope.fileURI, encodeURI("http://pixoloproductions.com/fcb/fcbescolarest/index.php/teams/uploadImage"), win, fail, options, true);

            } else {
                submitpost();
            }


        }

    })

    .controller('categoriesCtrl', function ($scope, $rootScope, $location, $interval, $ionicLoading, $stateParams, ApiServices, $ionicSideMenuDelegate) {

        /*INITIALIZATION*/



        var loadingpopup = ApiServices.showloadingpopup();

        $ionicSideMenuDelegate.canDragContent(true);

        getalldatasuccess = function (response) {
            console.log(response);
            loadingpopup.close();
            $scope.categories = [];
            $scope.categories = response.data;
            console.log($scope.categories);
        }

        getalldataerror = function (error) {
            console.log(error);
            loadingpopup.close();
        }


        $scope.$on('$ionicView.enter', function () {
            var tournament_id = 24;



            ApiServices.getmanyby('categories', 'tournament_id', tournament_id).then(getalldatasuccess, getalldataerror);

        });






    })

    .controller('timelineCtrl', function ($scope, $rootScope, $location, $interval, $ionicLoading, $stateParams, ApiServices, $ionicScrollDelegate, $cordovaInAppBrowser, $ionicPopup) {

        console.log($rootScope.fcbuserdata);
        /*INITIALIZATION*/
        $scope.timelineposts = [];
        $scope.platform = ionic.Platform.platform();
        console.log($scope.platform);
        $scope.showlikebutton = false;
        console.log($scope.platform)
        // GET LIST OF ALL THE PLAGES LOKE BY THE USER ON FACEBOOK
        var getlikedpagessuccess = function (response) {
            //            FCBEscola Mumbai
            console.log(response.data.data);

            var index = response.data.data.findIndex(likedpage => likedpage.name.toLowerCase() == 'FCBEscola Mumbai'.toLowerCase());


            console.log(index);
            if (index != -1) {
                $scope.showlikebutton = false;
            } else {
                $scope.showlikebutton = true;
            };
        };
        var getlikedpageserror = function (response) {
            console.log(response.data);
        };
        // CHECK IF ACCESS TOKEN EXISTS
        if ($rootScope.fcbuserdata.facebook_access_token != '') {
            $scope.showloginbutton = false;
            //            ApiServices.getlikedpages($rootScope.fcbuserdata.facebook_access_token).then(getlikedpagessuccess, getlikedpageserror);
        } else {
            $scope.showloginbutton = true;
        };


        var gettimelinepost = function () {
            console.log('getting post');
            $scope.showspinner = true;
            getallstoriessuccess = function (response) {

                console.log(response);
                $scope.timelineposts = response.data;
                setTimeout(function () {
                    console.log('settimeout');
                    $scope.showspinner = false;
                    $scope.$apply();
                }, 1000);

            }

            getallstorieserror = function (error) {
                $scope.showspinner = false;
                console.log(error);
            }
            ApiServices.getallstories().then(getallstoriessuccess, getallstorieserror);
        }

        /*SCOPE FUNCTIONS*/
        $scope.getstories = function () {
            if ($ionicScrollDelegate.getScrollPosition().top == 0) {
                gettimelinepost();
            }

        };
        $scope.sharepost = function (post) {
            console.log(post);
        };
        $scope.deletepost = function (post) {
            console.log(post);
            deletedatabyidsuccess = function (response) {
                console.log(response);
                if (response.data) {
                    $scope.timelineposts.splice($scope.timelineposts.indexOf(post), 1);

                }
            };

            deletedatabyiderror = function (error) {
                console.log(error);
            };

            ApiServices.deletedatabyid('stories', post.id).then(deletedatabyidsuccess, deletedatabyiderror);
        };
        $scope.reportpost = function (post) {

            reportstorysuccess = function (response) {
                ApiServices.showpopup("Report", "You have reported this post.Please wait for admin's response.");

                console.log(response);
            }

            reportstoryerror = function (error) {
                console.log(error);
            }
            ApiServices.reportstory(post.id).then(reportstorysuccess, reportstoryerror);
            console.log(post);
        };




        $scope.gotoaddstorypage = function () {
            /*Check if user is logged in or not via facebook */
            if ($.jStorage.get('fcbuserdata') && $.jStorage.get('fcbuserdata').facebook_id != '') {
                $location.path('app/addstory');
            } else {
                ApiServices.showpopup('Stop right there', 'Sorry ! We cannot let you go any further without logging in via facebook.');
                console.log('You are not logged in via facebook !');
            }

        };


        $scope.$on('$ionicView.enter', function () {
            gettimelinepost();

        });

        var options = {
            location: 'yes',
            clearcache: 'yes',
            toolbar: 'no'
        };
        $rootScope.$on('$cordovaInAppBrowser:exit', function (e, event) {
            console.log(e);
            console.log(event);
        });


        $scope.openpagetolike = function () {
            $cordovaInAppBrowser.open('https://www.facebook.com/v3.0/plugins/error/confirm/like?iframe_referer=http%3A%2F%2Flocalhost%3A8100%2F&kid_directed_site=false&secure=true&plugin=like&return_params=%7B%22action%22%3A%22like%22%2C%22app_id%22%3A%22626608564344145%22%2C%22channel%22%3A%22http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FRQ7NiRXMcYA.js%3Fversion%3D42%23cb%3Dfbe83976122ec4%26domain%3Dlocalhost%26origin%3Dhttp%253A%252F%252Flocalhost%253A8100%252Ff2e190d6d15e614%26relation%3Dparent.parent%22%2C%22container_width%22%3A%22360%22%2C%22href%22%3A%22https%3A%2F%2Fwww.facebook.com%2FFCBEscolaMUMB%2F%22%2C%22layout%22%3A%22box_count%22%2C%22locale%22%3A%22en_US%22%2C%22sdk%22%3A%22joey%22%2C%22share%22%3A%22true%22%2C%22show_faces%22%3A%22true%22%2C%22size%22%3A%22large%22%2C%22ret%22%3A%22sentry%22%2C%22act%22%3A%22like%22%7D', '_blank', options)

                .then(function (event) {
                    // success
                    console.log(event);
                })

                .catch(function (event) {
                    // error
                    console.log(event);
                });


        };




        $scope.showphotopopup = function (imagename) {
            $scope.photos = [];
            $scope.photos.push({
                name: imagename
            });
            $scope.path = '';
            /* OPEN POPUP */
            var photoPopup = $ionicPopup.show({
                cssClass: 'photo-popup',
                templateUrl: 'templates/photopopup.html',
                scope: $scope
            });

            //                
            setTimeout(function () {

                $('.photo-popup').click(function () {
                    photoPopup.close();
                    $scope.$apply();

                });


            }, 0);

        }



    })
    .controller('groupsCtrl', function ($scope, $rootScope, $location, $interval, $ionicLoading, $stateParams, ApiServices) {


        var categoryid = $stateParams.categoryid;
        $scope.pointstabledata = [];



        getdataforpointstablesuccess = function (response) {
            console.log(response);
            $scope.pointstabledata = response.data;
        }

        getdataforpointstableerror = function (error) {
            console.log(error);
        }



        ApiServices.getdataforpointstable(categoryid).then(getdataforpointstablesuccess, getdataforpointstableerror);

    })


    .controller('teamsCtrl', function ($scope, $rootScope, $location, $interval, $ionicLoading, $stateParams, ApiServices) {

        /*INITIALIZATION*/
        $scope.teams = [];
        $scope.userfollowteam = {};


        var loadingpopup = ApiServices.showloadingpopup();
        /*GET ALL TEAMS FROM INFORMATION FOR FIXTURES*/
        var setteamsarray = function () {
            console.log($rootScope.dataforfixture);
            _.forEach($rootScope.dataforfixture, function (dataforfixturevalue, dataforfixtureindex) {

                _.forEach(dataforfixturevalue.groups, function (groupsvalue, groupsindex) {
                    $scope.teams = $scope.teams.concat(groupsvalue.teams);
                });
                console.log($scope.teams);
            });

        };

        //        setteamsarray();


        /*CALLBACKS*/


        /*   getalldatasuccess = function (response) {
                   console.log(response);
                   $scope.teams = response.data;

               }
               getalldataerror = function (error) {
                   console.log(error);
               }*/

        /*SCOPE FUNCTIONS*/
        var followanimation = function (team, userfollowid, eventtarget) {
            console.log(eventtarget);

            var element = eventtarget.target;



            //SET FOLLOWING VALUE OF TEAM AS TRUE HERE
            $(element).css('color', 'white');
            $(element).html('')
            $(element).css({
                'padding': '0',
                'width': '14px'
            });

            $(element).addClass('continous-rotate');

            var self = $(element);
            var tickid = $(element).data('tickid');


            //CALL THIS DURING SUCCESS
            setTimeout(function () {
                console.log("ÁAA");
                self.fadeOut();
                console.log(tickid);
                setTimeout(function () {

                    $('#' + tickid).show();
                    $('#' + tickid).css({
                        'height': '35px',
                        'width': '35px',
                        'transform': 'rotateZ(360deg)',
                        'opacity': '1'
                    })

                }, 400);
                $scope.userfollowteam[team.id].id = userfollowid;
            }, 800);
            //END OF SUCCESS







        }









        $scope.followteam = function (team, eventtarget) {

            $scope.userfollowteam[team.id] = {
                user_id: $.jStorage.get('fcbuserdata').id,
                team_id: team.id
            };

            storepostdatasuccess = function (response) {
                //            console.log(response);
                followanimation(team, response.data, eventtarget);
            }
            storepostdataerror = function (error) {
                console.log(error);
            }

            //STORE DATA IN USER_FOLLOW TABLE

            ApiServices.storepostdata('User_followteam', $scope.userfollowteam[team.id]).then(storepostdatasuccess, storepostdataerror);






        };





        var unfollowanimation = function (teamid, event) {
            //      

            var index = event.path.findIndex(path => $(path).is('a') == true);

            var element = angular.copy(event.path[index]);
            console.log(index, element);
            var tickid = $(element).attr('id');


            $('*[data-tickid="' + tickid + '"]').css('color', 'white');
            $('*[data-tickid="' + tickid + '"]').html('')
            $('*[data-tickid="' + tickid + '"]').css({
                'padding': '0',
                'width': '14px'
            });

            $('*[data-tickid="' + tickid + '"]').addClass('continous-rotate');



            console.log(event);
            //            var element = event.target;

            var self = $(element);

            console.log(self);
            $('#' + tickid).css({
                'height': '0px',
                'width': '0px',
                'transform': 'rotateZ(720deg)',
                'opacity': '0'
            });


            setTimeout(function () {
                self.hide();
                $(element).css('color', 'white');
                $(element).html('');
                $(element).css({
                    'padding': '0',
                    'width': '14px'
                });
                $(element).addClass('continous-rotate');

                $('*[data-tickid="' + tickid + '"]').show();


                // CALL ON SUCCESS
                setTimeout(function () {
                    $('*[data-tickid="' + tickid + '"]').removeClass('continous-rotate');

                    $('*[data-tickid="' + tickid + '"]').css({
                        'color': '#000',
                        'padding': '3px 20px',
                        'width': 'auto'
                    });
                    $('*[data-tickid="' + tickid + '"]').html('Follow');
                }, 400);
                delete $scope.userfollowteam[teamid];
                $scope.$apply();
                console.log(teamid);
                console.log($scope.userfollowteam);

            }, 400);

        }

        $scope.unfollowteam = function (teamid, event) {

            console.log(teamid);
            console.log($scope.userfollowteam);

            console.log($scope.userfollowteam[teamid]);


            deletebyteamanduseridsuccess = function (response) {
                console.log(response);
                //SET FOLLOWING VALUE OF TEAM AS FALSE HERE
                unfollowanimation(teamid, event);

            }
            deletebyteamanduseriderror = function (error) {
                console.log(error);
            }



            ApiServices.deletedatabyid('User_followteam', $scope.userfollowteam[teamid].id).then(deletebyteamanduseridsuccess, deletebyteamanduseriderror);
        };



        /*GET THE ALL TEAMS FROM TABLE TO FOLLOW*/
        //        ApiServices.getteamstofollow().then(getalldatasuccess, getalldataerror);

        getmanybysuccess = function (response) {
            console.log(response);
            _.forEach(response.data, function (userfollow, key) {
                for (var cat in $rootScope.dataforfixture) {
                    for (group in $rootScope.dataforfixture[cat].groups) {
                        var index = $rootScope.dataforfixture[cat].groups[group].teams.findIndex(team => team.id == userfollow.team_id);

                        if (index != -1) {
                            console.log(userfollow.team_id);
                            setTimeout(function () {
                                $('#tick' + userfollow.team_id).show();
                                $('#tick' + userfollow.team_id).css({
                                    'height': '35px',
                                    'width': '35px',
                                    'transform': 'rotateZ(360deg)',
                                    'opacity': '1'
                                })
                            }, 0);


                        }

                        $scope.userfollowteam[userfollow.team_id] = userfollow;


                    }
                }
            });
            console.log($scope.userfollowteam);
            loadingpopup.close();
        }

        getmanybyerror = function (error) {
            console.log(error);
            loadingpopup.close();
        }
        $scope.$on('$ionicView.enter', function () {
            if ($.jStorage.get('fcbuserdata')) {
                ApiServices.getmanyby('User_followteam', 'user_id', $.jStorage.get('fcbuserdata').id).then(getmanybysuccess, getmanybyerror);
            }
        });



    })

    //followingCtrl
    .controller('followingCtrl', function ($scope, $rootScope, $location, $interval, $ionicLoading, $stateParams, ApiServices) {

        var loadingpopup = ApiServices.showloadingpopup();

        /*CallBacks*/
        getfixturesoffollowingteamsuccess = function (response) {
            console.log(response);
            loadingpopup.close();
            $scope.followingteamsinformation = response.data;

        }

        getfixturesoffollowingteamerror = function (error) {
            console.log(error);
            loadingpopup.close();
        }






        $scope.$on('$ionicView.enter', function () {
            $scope.followingteamsinformation = [];



            ApiServices.getfixturesoffollowingteam().then(getfixturesoffollowingteamsuccess, getfixturesoffollowingteamerror);
        });


    })
