// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'apiservices', 'ngCordova', 'ngCordovaOauth'])

    .run(function ($ionicPlatform, ApiServices, $location) {
        $ionicPlatform.ready(function () {


            // Enable to debug issues.
            // window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});

            var notificationOpenedCallback = function (jsonData) {
                console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
            };

            if (window.location.hostname != "localhost") {
                window.plugins.OneSignal
                    .startInit("bc6584d9-6c8c-462c-a779-632922e314c2")
                    .handleNotificationOpened(notificationOpenedCallback)
                    .endInit();
            };

            if (window.location.hostname != "localhost") {
                if (typeof analytics !== undefined) {
                    analytics.startTrackerWithId("UA-120150807-1");

                    var anuser = $.jStorage.get("user");
                    if (!anuser || anuser == {}) {

                    } else {
                        analytics.setUserId(anuser.id);
                    };
                } else {
                    console.log("Google Analytics Unavailable");
                };
            };

            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            /*if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
            }*/

            //            if (window.StatusBar) {
            //                // org.apache.cordova.statusbar required
            //                StatusBar.styleDefault();
            //            }


        });






       
        //
    })

    .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
        $stateProvider





            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'AppCtrl'
            })
            .state('app.teams', {
                url: '/teams',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/teams.html',
                        controller: 'teamsCtrl',
                    }
                }
            })
            .state('app.groups', {
                url: '/groups/:categoryid',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/groups.html',
                        controller: 'groupsCtrl',
                    }
                }
            })
            .state('app.timeline', {
                url: '/timeline',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/timeline.html',
                        controller: 'timelineCtrl',
                    }
                }
            })
            .state('app.categories', {
                url: '/categories',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/categories.html',
                        controller: 'categoriesCtrl',
                    }
                }
            })
            .state('app.addstory', {
                url: '/addstory',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/addstory.html',
                        controller: 'addstoryCtrl',
                    }
                }
            })
            .state('app.pointstable', {
                url: '/pointstable',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/pointstable.html',
                        controller: 'pointstableCtrl',
                    }
                }
            })
            .state('app.login', {
                url: '/login',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/login.html',
                        controller: 'loginCtrl',
                    }
                }
            })
            .state('app.fixtures', {
                url: '/fixtures/:catid',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/fixtures.html',
                        controller: 'fixturesCtrl',
                    }
                }
            })

            .state('app.album', {
                url: '/album',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/album.html',
                        controller: 'albumCtrl',
                    }
                }
            })


            .state('app.following', {
                url: '/following',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/following.html',
                        controller: 'followingCtrl',
                    }
                }
            })
            .state('app.categoriesforfixtures', {
                url: '/fixturescategories',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/categoriesforfixtures.html',
                        controller: 'categoriesCtrl',

                    }
                }
            })

            .state('app.rules', {
                url: '/rules',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/rules.html'

                    }
                }
            })

            .state('app.about', {
                url: '/about',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/about.html'

                    }
                }
            })





        ;
        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise($.jStorage.get('fcbuserdata') ? '/app/timeline' : '/app/login');
        $ionicConfigProvider.views.transition('none');


    })

    .filter('imagepath', function () {
        return function (imagename, path) {
            if (path != '') {

                if (path == 'teams') {
                    if (imagename != '') {
                        return "http://pixoloproductions.com/fcb/fcbescolarest/uploads/" + path + "/" + imagename;
                    } else {
                        return "img/team_logo.png";
                    }
                } else {
                    return "http://pixoloproductions.com/fcb/fcbescolarest/uploads/" + path + "/" + imagename;
                }
            } else {
                return "http://pixoloproductions.com/fcb/fcbescolarest/uploads/" + imagename;

            }
        }
    })
    .filter('checkpath', function ($location) {
        return function (value) {

            console.log(value);
            return $location.path().includes(value);
        }
    })
    .filter('facebookprofile', function () {
        return function (fbid) {


            return "http://graph.facebook.com/" + fbid + "/picture?width=50&height=50";
        }
    })
    .directive('errSrc', function () {
        return {
            link: function (scope, element, attrs) {
                element.bind('error', function () {
                    if (attrs.src != attrs.errSrc) {
                        attrs.$set('src', attrs.errSrc);
                    }
                });
            }
        }
    })

    .filter('youtubetrusted', ['$sce', function ($sce) {
        return function (url) {

            var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
            var match = url.match(regExp);
            var video_id = (match && match[7].length == 11) ? match[7] : false;

            return $sce.trustAsResourceUrl("https://www.youtube.com/embed/" + video_id);
        }
    }])

    .run(function ($ionicPlatform, ApiServices, $location, $rootScope, $ionicHistory) {
        /*CHANGE BACK BUTTON FUNCTIONALITY*/

        $ionicPlatform.registerBackButtonAction(function (e) {
            console.log('Button pressed', $location.path());
            var loop1 = false;

            if ($rootScope.backButtonPressedOnceToExit) {
                ionic.Platform.exitApp();
            } else {
                if ($location.path() == '/app/login') {
                    console.log($location.path());
                    loop1 = true;
                    $rootScope.backButtonPressedOnceToExit = true;
                    ApiServices.showtoast(
                        "Press back button again to exit", 'long', 'center');
                    setTimeout(function () {
                        $rootScope.backButtonPressedOnceToExit = false;
                    }, 2000);
                };
                if ($location.path() == '/app/fixturescategories') {
                    console.log($location.path());
                    loop1 = true;
                    $rootScope.backButtonPressedOnceToExit = true;
                    ApiServices.showtoast(
                        "Press back button again to exit", 'long', 'center');
                    setTimeout(function () {
                        $rootScope.backButtonPressedOnceToExit = false;
                    }, 2000);
                };
                if ($location.path() == '/app/timeline') {
                    console.log($location.path());
                    loop1 = true;
                    $rootScope.backButtonPressedOnceToExit = true;
                    ApiServices.showtoast(
                        "Press back button again to exit", 'long', 'center');
                    setTimeout(function () {
                        $rootScope.backButtonPressedOnceToExit = false;
                    }, 2000);
                };
                if ($location.path() == '/app/album') {
                    console.log($location.path());
                    loop1 = true;
                    $rootScope.backButtonPressedOnceToExit = true;
                    ApiServices.showtoast(
                        "Press back button again to exit", 'long', 'center');
                    setTimeout(function () {
                        $rootScope.backButtonPressedOnceToExit = false;
                    }, 2000);
                };
                if ($location.path() == '/app/categories') {
                    console.log($location.path());
                    loop1 = true;
                    $rootScope.backButtonPressedOnceToExit = true;
                    ApiServices.showtoast(
                        "Press back button again to exit", 'long', 'center');
                    setTimeout(function () {
                        $rootScope.backButtonPressedOnceToExit = false;
                    }, 2000);
                };
            };

            if (loop1 == false) {
                if ($rootScope.backButtonPressedOnceToExit) {
                    ionic.Platform.exitApp();
                } else if ($ionicHistory.backView()) {
                    $ionicHistory.goBack();
                } else {
                    window.location = "index.html#/app/timeline"
                    $rootScope.backButtonPressedOnceToExit = true;
                    ApiServices.showtoast(
                        "Press back button again to exit", 'long', 'center');
                    setTimeout(function () {
                        $rootScope.backButtonPressedOnceToExit = false;
                    }, 2000);

                }
            };

            e.preventDefault();
            return false;
        }, 101);



    });
